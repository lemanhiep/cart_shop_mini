package com.cart_shop_mini.controller.api;

import com.cart_shop_mini.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/rest")
public class AccountController {
    @Autowired
//    AccountService accountService;


    @GetMapping("/accounts")
    public ResponseEntity<?> getListAccount()
    {
        return null;
    }
    @PostMapping("/accounts")
    public ResponseEntity<?> createAccount()
    {
        return null;
    }
    @PutMapping("/accounts/{id}")
    public ResponseEntity<?> updateAccount()
    {
        return null;
    }
    @DeleteMapping("/accounts/{id}")
    public ResponseEntity<?> deleteAccount()
    {
        return null;
    }
}

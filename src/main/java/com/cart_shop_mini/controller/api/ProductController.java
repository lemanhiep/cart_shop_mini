package com.cart_shop_mini.controller.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.cart_shop_mini.service.ProductService;

@RestController
@RequestMapping("/api/product")

public class ProductController {
    @Autowired
    ProductService productService;

    @GetMapping
    public ResponseEntity listAllProducts(@RequestParam(name = "page") int page, @RequestParam(name = "size") int size) {
        return ResponseEntity.ok(productService.listAllProducts(page, size));
    }
}

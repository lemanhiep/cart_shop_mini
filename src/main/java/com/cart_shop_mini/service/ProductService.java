package com.cart_shop_mini.service;

import com.cart_shop_mini.entity.Product;

import java.util.List;

public interface ProductService {
    List<Product> listAllProducts(int offset, int pageSize);
}

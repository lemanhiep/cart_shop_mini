package com.cart_shop_mini.service.impl;

import com.cart_shop_mini.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import com.cart_shop_mini.repository.ProductRepo;
import com.cart_shop_mini.service.ProductService;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {
    @Autowired
    ProductRepo productRepo;

    @Override
    public List<Product> listAllProducts(int offset, int pageSize) {
        Page<Product> products = productRepo.findAll(PageRequest.of(offset,pageSize));
        List<Product> products1 = products.getContent();
        return products1;
    }
}

package com.cart_shop_mini.repository;

import com.cart_shop_mini.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepo extends JpaRepository<Product, Integer> {
}

package com.cart_shop_mini;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CartShopMiniApplication {

    public static void main(String[] args) {
        SpringApplication.run(CartShopMiniApplication.class, args);
    }

}
